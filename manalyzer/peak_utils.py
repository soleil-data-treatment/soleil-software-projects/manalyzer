#!/usr/bin/env python

import sys, os
import time, datetime

import numpy
from scipy import optimize

from utils import reading_data

################################
PEAK_RADIUS = 2.
INIT_SIGMA = 0.05  # gaussian
################################
fitfunc = {
'gaussian': lambda p, x: p[3]+ p[0]*numpy.exp(-(x-p[1])*(x-p[1])/(2*p[2]*p[2])), 
'lorentzian': lambda p, x: p[3]+ p[0]*p[2]/((x-p[1])*(x-p[1])+p[2]*p[2]) 
}

#####################################
def fit_peak(angles, vals, func_name):
    errfunc = lambda p, x, y: fitfunc[func_name](p, x) - y 
        
    max_val = max(vals)
    center = angles[numpy.argmax(vals)]
    p0 = [max_val, center, INIT_SIGMA, 0.] # Initial guess 
    
    params, success = optimize.leastsq(errfunc, p0[:], args=(angles, vals))
    
    return params, success
    
#def locate_peak
########################################
def localize_highest_peak(rx2, dtr_data, fit_type = 'gaussian'):

    target_func = lambda p, x: p[0]*numpy.exp(-(x-p[1])*(x-p[1])/(p[2]*p[2])) # Target function
    #target_func = fitfunc[fit_type]
    errfunc = lambda p, x, y: target_func(p, x) - y # Distance to the target function

##    print "\nIn peak_utils.localize_highest_peak:" # debug

    uniform_step = (rx2[-1]-rx2[0])/len(rx2)
    indx_radius = int(PEAK_RADIUS/uniform_step)
##    print 'indx_radius =', indx_radius # debug
    
    max_indx = numpy.argmax(dtr_data)
    
    leftBound_indx = max(0, max_indx - indx_radius)
    rightBound_indx = min(len(rx2)-1, max_indx + indx_radius)
    
    max_val = max(dtr_data)
    center = rx2[max_indx]
    p0 = [max_val, center, INIT_SIGMA] # Initial guess 

##    print 'len(rx2) =', len(rx2) # debug
##    print len(rx2[leftBound_indx: rightBound_indx]) # debug
##    print leftBound_indx, rightBound_indx # debug
##    print rx2[leftBound_indx: rightBound_indx] # debug
    p, success = optimize.leastsq( errfunc, p0[:],
                                   args=(rx2[leftBound_indx: rightBound_indx],
                                         dtr_data[leftBound_indx: rightBound_indx]) )
##    print # debug
    return p, success


#############################################################
def externalROI(dtr_data, rx2, left_bound, right_bound):

    leftBound_indx = numpy.argmin( numpy.abs(rx2 - left_bound) )
    rightBound_indx = numpy.argmin( numpy.abs(rx2 - right_bound) )

    

    return numpy.concatenate( (dtr_data[:leftBound_indx],
                               numpy.zeros(rightBound_indx - leftBound_indx),
                               dtr_data[rightBound_indx:]) )

#############################
if __name__ == '__main__':

    '----------- Reading data file ------------'

    if len(sys.argv) < 2:
        print ("\nCorrect use: " + (sys.argv[0]).split('/')[-1] + " DATAFILE ")
        sys.exit(1)


    filename= sys.argv[1]
    
    splitted_data_path = filename.split(os.sep)
    data_path = ''
    if len(splitted_data_path) > 1:
        for s in splitted_data_path[:-1]:
            data_path += s + os.sep
    else:
        data_path = './'
    #print data_path # debug


    print ("Reading data: "+ str(datetime.datetime.now()))
    rx2, dtrs_data = reading_data(filename)
    print ("Reading data DONE: "+ str(datetime.datetime.now()) +'\n')

    p,s  = localize_highest_peak(rx2, dtrs_data[0])
    
    # --------- TEST localize_highest_peak -----------
    print (str(p)) # debug
    leftBound_indx = numpy.argmin( numpy.abs(rx2 - (p[1]- 5*p[2])) )
    rightBound_indx = numpy.argmin( numpy.abs(rx2 - (p[1]+ 5*p[2])) )


    x = rx2[leftBound_indx: rightBound_indx+1]
    numpy.savetxt(data_path + 'gauss.dat',
                  numpy.transpose( numpy.array( [x, p[0]*numpy.exp(-(x-p[1])*(x-p[1])/(p[2]*p[2])) ])), fmt = '%12.6g')
    # --------- TEST localize_highest_peak done -----------


    # --------- TEST externalROI -----------
    dtr_data_cut1 = externalROI( dtrs_data[0], rx2, p[1]- 5*p[2], p[1]+ 5*p[2])

    numpy.savetxt(data_path + 'cut1.dat',
                  numpy.transpose( numpy.array([rx2, dtr_data_cut1]) ), fmt = '%12.6g')

##    for d in range (len(shifts)): 
##        shifts_file.write( 'DT'+str(d+1) + ':\t%8.6f\n' % (shifts[d]) )



