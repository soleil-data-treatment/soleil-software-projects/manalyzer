#!/usr/bin/env python

#########################################################################
import os
import sys
import math

EPSILON = 0.0000001

#########################################################################
def coeffs(neighbor1_pos, neighbor2_Pos, neighbor1_val, neighbor2_val):

    a = (neighbor2_val-neighbor1_val)/( neighbor2_Pos - neighbor1_pos);
    b = neighbor1_val-a*neighbor1_pos;

    return a,b

#########################################################################
def get_intermediatePos(intermediateVal, neighbor1_pos, neighbor2_Pos, neighbor1_val, neighbor2_val): 

    # if _hm does not lie between neighbor1_val and  neighbor2_val, it does not make sense
    a, b = coeffs(neighbor1_pos, neighbor2_Pos, neighbor1_val, neighbor2_val)

    return (intermediateVal-b)/a

#########################################################################
def get_intermediateVal(intermediatePos, neighbor1_pos, neighbor2_pos, neighbor1_val, neighbor2_val): 

    # if _hm does not lie between neighbor1_val and  neighbor2_val, it does not make sense
    a, b = coeffs(neighbor1_pos, neighbor2_pos, neighbor1_val, neighbor2_val)

    return a*intermediatePos+b


##########################################################
def interpolate(new_x, x, y):
    """
    return  new_y array
    """

    if new_x[0] < x[0]-EPSILON or new_x[-1] > x[-1]+EPSILON:
        print ("In linear_interpolation.interpolate: Invalid interpolation bounds")
        sys.exit(0)

    new_y = []

    j = 0
    for i in range (1,len(x)):
        while new_x[j] <= x[i] and j < len(new_x)-1:
            new_y.append( get_intermediateVal(new_x[j], x[i-1], x[i], y[i-1], y[i]) )
            j +=1
        if j == len(new_x)-1:
            new_y.append( get_intermediateVal(new_x[j], x[i-1], x[i], y[i-1], y[i]) )
            break
    return new_y
